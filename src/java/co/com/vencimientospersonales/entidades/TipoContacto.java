/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientospersonales.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alejandro Tamayo
 */
@Entity
@Table(name = "tipo_contacto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoContacto.findAll", query = "SELECT t FROM TipoContacto t")
    , @NamedQuery(name = "TipoContacto.findByIdTipoContacto", query = "SELECT t FROM TipoContacto t WHERE t.idTipoContacto = :idTipoContacto")
    , @NamedQuery(name = "TipoContacto.findByNombreTipoContacto", query = "SELECT t FROM TipoContacto t WHERE t.nombreTipoContacto = :nombreTipoContacto")
    , @NamedQuery(name = "TipoContacto.findByDescripcionTipoContacto", query = "SELECT t FROM TipoContacto t WHERE t.descripcionTipoContacto = :descripcionTipoContacto")})
public class TipoContacto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_contacto")
    private Integer idTipoContacto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombre_tipo_contacto")
    private String nombreTipoContacto;
    @Size(max = 100)
    @Column(name = "descripcion_tipo_contacto")
    private String descripcionTipoContacto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoContacto")
    private List<Contacto> contactoList;

    public TipoContacto() {
    }

    public TipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public TipoContacto(Integer idTipoContacto, String nombreTipoContacto) {
        this.idTipoContacto = idTipoContacto;
        this.nombreTipoContacto = nombreTipoContacto;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public String getNombreTipoContacto() {
        return nombreTipoContacto;
    }

    public void setNombreTipoContacto(String nombreTipoContacto) {
        this.nombreTipoContacto = nombreTipoContacto;
    }

    public String getDescripcionTipoContacto() {
        return descripcionTipoContacto;
    }

    public void setDescripcionTipoContacto(String descripcionTipoContacto) {
        this.descripcionTipoContacto = descripcionTipoContacto;
    }

    @XmlTransient
    public List<Contacto> getContactoList() {
        return contactoList;
    }

    public void setContactoList(List<Contacto> contactoList) {
        this.contactoList = contactoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoContacto != null ? idTipoContacto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoContacto)) {
            return false;
        }
        TipoContacto other = (TipoContacto) object;
        if ((this.idTipoContacto == null && other.idTipoContacto != null) || (this.idTipoContacto != null && !this.idTipoContacto.equals(other.idTipoContacto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nombreTipoContacto; }
    
}
