/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientospersonales.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alejandro Tamayo
 */
@Entity
@Table(name = "vencimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vencimiento.findAll", query = "SELECT v FROM Vencimiento v")
    , @NamedQuery(name = "Vencimiento.findByIdVencimiento", query = "SELECT v FROM Vencimiento v WHERE v.idVencimiento = :idVencimiento")
    , @NamedQuery(name = "Vencimiento.findByNombreVencimiento", query = "SELECT v FROM Vencimiento v WHERE v.nombreVencimiento = :nombreVencimiento")
    , @NamedQuery(name = "Vencimiento.findByFecha", query = "SELECT v FROM Vencimiento v WHERE v.fecha = :fecha")
    , @NamedQuery(name = "Vencimiento.findByHora", query = "SELECT v FROM Vencimiento v WHERE v.hora = :hora")})
public class Vencimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_vencimiento")
    private Integer idVencimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombre_vencimiento")
    private String nombreVencimiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "hora")
    @Temporal(TemporalType.TIME)
    private Date hora;
    @JoinColumn(name = "categoria", referencedColumnName = "id_categoria")
    @ManyToOne(optional = false)
    private Categoria categoria;
    @OneToMany(mappedBy = "vencimiento")
    private List<Usuario> usuarioList;

    public Vencimiento() {
    }

    public Vencimiento(Integer idVencimiento) {
        this.idVencimiento = idVencimiento;
    }

    public Vencimiento(Integer idVencimiento, String nombreVencimiento, Date fecha) {
        this.idVencimiento = idVencimiento;
        this.nombreVencimiento = nombreVencimiento;
        this.fecha = fecha;
    }

    public Integer getIdVencimiento() {
        return idVencimiento;
    }

    public void setIdVencimiento(Integer idVencimiento) {
        this.idVencimiento = idVencimiento;
    }

    public String getNombreVencimiento() {
        return nombreVencimiento;
    }

    public void setNombreVencimiento(String nombreVencimiento) {
        this.nombreVencimiento = nombreVencimiento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVencimiento != null ? idVencimiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vencimiento)) {
            return false;
        }
        Vencimiento other = (Vencimiento) object;
        if ((this.idVencimiento == null && other.idVencimiento != null) || (this.idVencimiento != null && !this.idVencimiento.equals(other.idVencimiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nombreVencimiento; }
    
}
