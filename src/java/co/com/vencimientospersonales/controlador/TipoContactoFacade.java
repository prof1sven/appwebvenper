/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientospersonales.controlador;

import co.com.vencimientospersonales.entidades.TipoContacto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alejandro Tamayo
 */
@Stateless
public class TipoContactoFacade extends AbstractFacade<TipoContacto> {

    @PersistenceContext(unitName = "vencimientos_personalesPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoContactoFacade() {
        super(TipoContacto.class);
    }
    
}
