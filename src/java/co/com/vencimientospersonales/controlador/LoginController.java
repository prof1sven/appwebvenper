/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.vencimientospersonales.controlador;

import co.com.vencimientospersonales.controlador.util.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author Alejandro Tamayo
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {
    private String nombreUsuario="";
    private String passwordUsuario="";

    /**
     * Creates a new instance of LoginController
     */
    public LoginController() {
    } 
    
    public String ingresar(){
        if(nombreUsuario.compareTo("vencimientos")== 0 && passwordUsuario.compareTo("vencimientos")==0){
            return "acceder";
        }else{
            
            JsfUtil.addErrorMessage("Datos Incorrectos");
            return null;
        }
                       
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPasswordUsuario() {
        return passwordUsuario;
    }

    public void setPasswordUsuario(String passwordUsuario) {
        this.passwordUsuario = passwordUsuario;
    }
    
    
    
    
}
